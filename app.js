const lista = document.getElementById('listaCompra');

let carrito = [
    {
        id: 198752,
        name: "Tinta DJ27 Color",
        price: 52.95,
        count: 3,
        premium: true
    },
    {
        id: 75621,
        name: "Impresora ticketera PRO-201",
        price: 32.75,
        count: 2,
        premium: true
    },
    {
        id: 54657,
        name: "Caja de rollos de papel de ticketera",
        price: 5.95,
        count: 3,
        premium: false
    },
    {
        id: 3143,
        name: "Caja de folios DIN-A4 80 gr",
        price: 9.95,
        count: 2,
        premium: false
    }
];


function mostrarCarrito(premium) {
    lista.innerHTML = "";
    let precio = 0;
    let giIncluidos = true;
    for (let i = 0; i < carrito.length; i++) {

        if((premium && carrito[i]['premium']) || !premium) {
            const li = document.createElement('li');
            let premiumProduct = "";

            if(carrito[i]['premium']) {
                premiumProduct = `<br><span class="productoPremium"> Producto Premium </span><br>`;
            } else {
                giIncluidos = false;
            }

            li.innerHTML = `<li>
                <div>
                    <span class="nombre">${carrito[i]['name']}</span><br>
                    <span class="precio"><b>Precio:</b> ${carrito[i]['price']}€</span><br>
                    <span class="cantidad"><b>Cantidad:</b> ${carrito[i]['count']}</span><br>
                    ${premiumProduct}
                    <button class="${carrito[i]['id']}" onclick="borrarProducto(${carrito[i]['id']});">Quitar del carrito</button>
                </div>
            </li>`;            
            precio += carrito[i]['price'] * carrito[i]['count'];
            lista.appendChild(li);

        }        
    }

    let descuento = "";
    if (precio > 50 ) {
        precio -= precio * 0.05;
        descuento = " (-5% por compras superiores a 50€)";
    }
    let gastosEnvio = ". Gastos de envío <b>no</b> incluídos.";
    if (giIncluidos) {
        if (precio > 0 ) {
            gastosEnvio = ". Gastos de envío incluídos.";
        } else {
            gastosEnvio = "";
        }
        
    }

    document.getElementById('total').innerHTML = "<b>Precio final</b> = " + precio.toFixed(2) + " €" +descuento + gastosEnvio;


}

function setPremium() {
    const checkboxPremium = document.getElementById('premium');
    if (checkboxPremium.checked) {
        mostrarCarrito(true);
    } else {
        mostrarCarrito(false);
    }
}

function borrarProducto(id) {
    let newCarrito = [];
    for (let i = 0; i < carrito.length; i++) {
        if (carrito[i]['id'] != id) {
            newCarrito.push(carrito[i]);
        }
        
    }
    carrito = newCarrito;
    setPremium();
}



console.log(carrito);
mostrarCarrito(false);